/**
 * Get dependencies
 */
const express = require('express'),
    path = require('path'),
    config = require('./config'),
    logger = require('morgan'),
    jwt = require('jsonwebtoken'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose');

/**
 * Use native Node promises
 */
mongoose.Promise = global.Promise;

/**
 * Connect to MongoDB
 */
mongoose.connect('mongodb://localhost/dizinext-test')
  .then(() => {
    console.log('MongoDB: connection succesful');
  })
  .catch((err) => console.error(err));

/**
 * Create App
 */
const app = express();

/**
 * Middlewares
 */
app.set('secretKey', config.secret);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
function checkToken(req, res, next) {
  // check header for token
  var token = req.headers['x-access-token'];

  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, app.get('secretKey'), (err, decoded) => {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });
  } else {
    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });
  }
}

/**
 * Routes
 */
app.use('/authenticate', require('./routes/authenticate'));
app.use('/', require('./routes/home'));
app.use('/tv-series', require('./routes/tvSeries'));
app.use('/tv-series-comments', require('./routes/tvSeriesComments'));
app.use('/seasons', require('./routes/seasons'));
app.use('/episode', require('./routes/episodes'));
app.use('/episode-comments', require('./routes/episodeComments'));
// Token Middleware
app.use((req, res, next) => {
  checkToken(req, res, next);
});
// Routes working with token
app.use('/posts', require('./routes/posts'));
app.use('/watch-later', require('./routes/watchLater'));
app.use('/post-comments', require('./routes/postComments'));
app.use('/users', require('./routes/users'));

/**
 * catch 404 and forward to error handler
 */
app.use((req, res, next) => {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/**
 * ERROR HANDLERS
 */

/**
 * development error handler
 * will print stacktrace
 */
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

/**
 * production error handler
 * no stacktraces leaked to user
 */
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err
  });
});

/**
 * Export app.
 */
module.exports = app;
