var Nightmare = require('nightmare');
var https = require('https');
var fs = require('fs');

var gremots = new Nightmare({show: true})
  .goto('https://gremots.com/dizi/sons-of-anarchy/sezon-2/bolum-1')
  .wait(500)
  .click('.skip-sf')
  .evaluate(function() {
    return $('.gmts-video video').attr('src')
  })
  .end()
  .then((result) => {
    console.log('result');
    console.log(result);

    var google = new Nightmare({show: true})
      .goto(result)
      .wait(500)
      .click('A')
      .end()
      .then((result) => {
        console.log('result');
        console.log(result);
      })
      .catch((error) => {
        console.error('Search failed:', error);
        var file = fs.createWriteStream("file.mp4");
        var request = https.get(error.url, function(response) {
          response.pipe(file);
        });
      })
  })
  .catch((error) => {
    console.error('Search failed:', error);
  })
