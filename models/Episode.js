/**
 * Get dependencies
 */
const mongoose = require('mongoose'),
      Season = require('./Season'),
      EpisodeComment = require('./EpisodeComment');

const EpisodeSchema = new mongoose.Schema({
  _ts_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'TvSeries'
  },
  _season_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Season'
  },
  name: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  },
  videoUrls: [{
    type: String
  }],
  subtitleUrls: [{
    type: String
  }]
});

/**
 * Virtual Populaters
 */
// Episode Comments
EpisodeSchema.virtual('episodeComments', {
  ref: 'EpisodeComment', // The model to use
  localField: '_id', // Find people where `localField`
  foreignField: '_ep_id' // is equal to `foreignField`
});

module.exports = mongoose.model('Episode', EpisodeSchema);
