/**
 * Get dependencies
 */
const mongoose = require('mongoose');

/**
 * Schema
 */
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  profilePhoto: {
    type: String,
    required: false
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  premiumUser: {
    type: Boolean,
    required: true,
    default: false
  },
  admin: {
    type: Boolean,
    required: true,
    default: false
  },
  editor: {
    type: Boolean,
    required: true,
    default: false
  },
  followers: {
    type: Array,
    required: false,
    default: []
  },
  following: {
    type: Array,
    required: false,
    default: []
  },
  watchedLength: {
    type: Number,
    default: 0
  },
  token: {
    type: String,
    required: false
  }
});

/**
 * Virtual Populaters
 */
// Posts
UserSchema.virtual('posts', {
  ref: 'Post', // The model to use
  localField: 'username', // Find people where `localField`
  foreignField: '_creator' // is equal to `foreignField`
});

// Post Comments
UserSchema.virtual('sentPostComments', {
  ref: 'PostComment', // The model to use
  localField: 'username', // Find people where `localField`
  foreignField: '_creator' // is equal to `foreignField`
});

// Saved for Watch Later
UserSchema.virtual('watchLaters', {
  ref: 'WatchLater', // The model to use
  localField: 'username', // Find people where `localField`
  foreignField: '_saver' // is equal to `foreignField`
});

// Tv Series Comments
UserSchema.virtual('sentTvSerieComments', {
  ref: 'TvSerieComment', // The model to use
  localField: 'username', // Find people where `localField`
  foreignField: '_creator' // is equal to `foreignField`
});

module.exports = mongoose.model('User', UserSchema);
