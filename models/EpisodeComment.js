/**
 * Get dependencies
 */
const mongoose = require('mongoose');

const EpisodeCommentSchema = new mongoose.Schema({
  _ep_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'TvSeries'
  },
  _creator: {
    type: String,
    ref: 'User'
  },
  text: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model('EpisodeComment', EpisodeCommentSchema);
