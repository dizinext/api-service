/**
 * Get dependencies
 */
const mongoose = require('mongoose'),
      Episode = require('./Episode'),
      User = require('./User');

const WatchLaterSchema = new mongoose.Schema({
  _saver: {
    type: String,
    ref: 'User'
  },
  _episode: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Episode'
  },
  note: {
    type: String,
    required: false
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

/**
 * Virtual Populaters
 */
// Episodes
WatchLaterSchema.virtual('episode', {
  ref: 'Episode', // The model to use
  localField: '_episode', // Find people where `localField`
  foreignField: '_id' // is equal to `foreignField`
});

module.exports = mongoose.model('WatchLater', WatchLaterSchema);
