/**
 * Get dependencies
 */
const mongoose = require('mongoose'),
      Episode = require('./Episode');

const SeasonSchema = new mongoose.Schema({
  _ts_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'TvSeries'
  },
  name: {
    type: String,
    required: true
  },
  number: {
    type: Number,
    required: true
  }
});

/**
 * Virtual Populaters
 */
// Episodes
SeasonSchema.virtual('episodes', {
 ref: 'Episode', // The model to use
 localField: '_id', // Find people where `localField`
 foreignField: '_season_id' // is equal to `foreignField`
});


module.exports = mongoose.model('Season', SeasonSchema);
