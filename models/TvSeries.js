/**
 * Get dependencies
 */
const mongoose = require('mongoose'),
      Episode = require('./Episode');

const TvSeriesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  posterUrl: {
    type: String,
    required: true
  },
  shortDescription: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  averageTime: {
    type: String,
    required: false
  },
  status: {
    type: String,
    required: false
  },
  imdbPoint: {
    type: Number,
    required: true,
    default: 0
  },
  country: {
    type: String,
    required: false
  },
  seasonLength: {
    type: Number,
    required: true
  },
  episodeLength: {
    type: Number,
    required: true
  },
  year: {
    type: String,
    required: true
  },
  followerLength: {
    type: Number,
    required: true,
    default: 0
  }
});

/**
 * Virtual Populaters
 */
// Comments For Tv Serie
TvSeriesSchema.virtual('tvSerieComments', {
  ref: 'TvSerieComment', // The model to use
  localField: '_id', // Find people where `localField`
  foreignField: '_ts_id' // is equal to `foreignField`
});

// Episodes
TvSeriesSchema.virtual('episodes', {
  ref: 'Episode', // The model to use
  localField: '_id', // Find people where `localField`
  foreignField: '_ts_id' // is equal to `foreignField`
});

// Seasons
TvSeriesSchema.virtual('seasons', {
  ref: 'Season', // The model to use
  localField: '_id', // Find people where `localField`
  foreignField: '_ts_id' // is equal to `foreignField`
});

module.exports = mongoose.model('TvSeries', TvSeriesSchema);
