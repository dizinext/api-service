/**
 * Get dependencies
 */
const mongoose = require('mongoose'),
      User = require('./User');

//
// TODO: Like Post
//

const PostSchema = new mongoose.Schema({
  _creator: {
    type: String,
    ref: 'User'
  },
  text: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

/**
 * Virtual Populaters
 */
// Posts
PostSchema.virtual('postComments', {
  ref: 'PostComment', // The model to use
  localField: '_id', // Find people where `localField`
  foreignField: '_post_id' // is equal to `foreignField`
});

module.exports = mongoose.model('Post', PostSchema);
