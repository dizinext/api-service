/**
 * Get dependencies
 */
const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
  _post_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Post'
  },
  _creator: {
    type: String,
    ref: 'User'
  },
  text: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model('PostComment', CommentSchema);
