/**
 * Get dependencies
 */
const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
  _ts_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'TvSeries'
  },
  _creator: {
    type: String,
    ref: 'User'
  },
  text: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model('TvSerieComment', CommentSchema);
