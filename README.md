# Dizinext

## Before Starting

On Development:
```bash
$ git clone https://username@bitbucket.org/dizinext/api-service.git && cd api-service
$ npm install && npm install nodemon -g
$ npm run start-dev
```

On Production:
```bash
$ git clone https://username@bitbucket.org/dizinext/api-service.git && cd api-service
$ npm install && npm install pm2 -g
$ npm run start-prod
```

## Google Drive String Converter Function

```javascript
  function DirectLinkConv(url) {
    return "https://drive.google.com/uc?id=" + String(String(String(url).split("d/")[1]).split('/view')[0])
  }
```

## General Notes:

### For Performance
* Use .lean() function for mongo queries, 'cause documents returned from queries with the lean option enabled are plain javascript objects, not MongooseDocuments. They have no save method, getters/setters or other Mongoose functions applied.

### For Api
* If url contains '?page=' query, it's only return 20 items with pagination.
* You must add x-access-token header to your requests for working with some routes like (users, posts) That's works with "Json web token" and HS256 Algorithm.

## Api Urls:

**Url**|** Method**|** Description**|** Datas**
:-----:|:-----:|:-----:|:-----:
http://dizinext.com/posts?page=0| GET| Get All Posts|
http://dizinext.com/posts/new| POST| Create New Post| \_creator: Creator Username, text: Post Text
http://dizinext.com/posts/{username}?page=0| GET| Get All Posts from specified user|
http://dizinext.com/posts/{id}?page=0| GET| Get Post from specified id|
http://dizinext.com/post-comments/{id}?page=0| GET| Get Post Comments from specified id|
http://dizinext.com/post-comments/user/{username}?page=0| GET| Get Post Comments from specified username|
http://dizinext.com/post-comments/new/{id}| POST| Create New Post Comment| \_creator: Creator Username, text: Comment
http://dizinext.com/tv-series?page=0| GET| Get Tv Series|
http://dizinext.com/tv-series/new| POST| Create New Tv Serie| name, shortDescription, description, averageTime, status, imdbPoint, country, seasonLength, episodeLength, year, followerLength
http://dizinext.com/tv-series/{id}?page=0| GET| Get Tv Serie from specified id|
http://dizinext.com/tv-series-comments/{id}?page=0| GET| Get Tv Serie Comments from specified id|
http://dizinext.com/tv-series-comments/new/{id}| POST| Create New Tv Serie Comment| \_creator: Creator Username, text: Comment
http://dizinext.com/episode/new/{id}| POST| Create New Episode|  \_ts_id, \_creator, name, videoUrls, subtitleUrls
http://dizinext.com/episode/{id}?page=0| GET| Get Episode from specified id|
http://dizinext.com/seasons/new/{id}| POST| Create New Season|  \_ts_id: Tv Serie Id, name: Like "1. Sezon", number: Season Number
http://dizinext.com/seasons/{id}?page=0| GET| Get Seasons from specified tv serie id|
http://dizinext.com/users?page=0| GET| Get All Users|
http://dizinext.com/users/new| POST| Create New User| name: His/Her Name, username: Username for His/Her, password: Password for His/Her, premiumUser: for specifying premium users
http://dizinext.com/users/{username}| GET| Get User|
http://dizinext.com/authenticate/authenticate/new| POST| Generate token| username, password
http://dizinext.com/authenticate/authenticate/check| POST| Check token| username, password, token

## Version Notes

### v1
* Profile:
  * Posts
  * Following TV Series
  * Profile Picture
  * Edit Profile
* User Actions:
  * Login
  * Logout
  * Forget Password
  * Register
* Social:
  * Self Details at sidebar like total post, follower, following length
  * Popular Users
  * Posts
    *	Sent Time
    * Like
    * Comment
    * Likers Length
* Navigations:
  * Explore (Home)
  * Movies
  * Tv Series
* Notifications Types:
  * Following TV Series New Episodes
  * Wanna watch new {Tv Series or Movie}
  * 100 People watching {Tv Series or Movie Name} now. Wanna watch?
* Explore (Home):
  * New Episodes
  * Featured Movies/Tv Series
  * Latest Watches
* Tv Series:
  * Follow Tv Series
  * Category
  * Ordering by: Follower
  * IMDB
  * Title
  * Year
* Tv Series Detail:
  * Description
  * Follower Length
  * Category
  * Season/Episode
  * Comments
  * Photo
* Tv Series Episode Detail:
  * Episode Name
	* Comments
  * Subtitle Notes
  * Languages for episode
  * Translation Rating
  * Translators
