var Utils = {
  isEmptyObject: function (object) {
    for (key in object) {
      return false;
    }
    return true;
  }
};

/**
 * Utils
 * @type {Object}
 */
module.exports = Utils;
