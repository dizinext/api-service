/**
 * Get dependencies
 */
const express = require('express'),
      Season = require('../models/Season'),
      router = express.Router();

/**
 * /seasons/new/:id - POST
 * Datas: _ts_id, name, number
 */
router.post('/new/:id', (req, res) => {

  // Creating Season
  var newSeason = new Season({
    _ts_id: req.params.id,
    name: req.body.name,
    number: req.body.number
  });

  newSeason
    .save()
    .then(ts => res.json({ "success": true, "episode": ts }) )
    .catch((err) => {
      console.log(err);
      res.json({
        "success": false,
        error: err
      });
    });
});

/**
 * /seasons/{id}?page=[Page Number - Starts from 0] - GET - Get TV Serie Season
 * Datas: id
 */
router.get('/:id', (req, res) => {
  /**
   * Get Season
   */
  Season
    .find({ _ts_id: req.params.id })
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .populate('episodes')
    .lean()
    .then((ts) => {
      if (ts) {
        res.json(ts);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});
module.exports = router;
