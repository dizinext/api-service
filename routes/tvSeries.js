/**
 * Get dependencies
 */
const express = require('express'),
      TvSeries = require('../models/TvSeries'),
      router = express.Router();

/**
 * /tv-series?page=[Page Number - Starts from 0] - GET
 *
 * Note: This function will return 20 items
 */
router.get('/', (req, res) => {

  /**
   * Find Tv Series
   */
  TvSeries
    .find({}, "-__v")
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .lean()
    .then((tvseries) => res.json(tvseries))
    .catch((err) => {
      console.log(err);
      res.json({ "error": true });
    });
});

/**
 * /tv-series/new - POST
 * Datas: name, shortDescription, description, averageTime, status, imdbPoint,
 * country, seasonLength, episodeLength, year, followerLength,
 */
router.post('/new', (req, res) => {

  // Creating Post
  var newTvSeries = new TvSeries({
    name: req.body.name,
    shortDescription: req.body.shortDescription,
    description: req.body.description,
    averageTime: req.body.averageTime,
    status: req.body.status,
    imdbPoint: req.body.imdbPoint,
    country: req.body.country,
    seasonLength: req.body.seasonLength,
    episodeLength: req.body.episodeLength,
    year: req.body.year,
    followerLength: req.body.followerLength
  });

  newTvSeries
    .save()
    .then(ts => res.json({ "success": true, "tvseries": ts }) )
    .catch((err) => {
      console.log(err);
      res.json({
        "success": false,
        error: err
      });
    });
});

/**
 * /tv-series/{id}?page=[Page Number - Starts from 0] - GET
 * Datas: id
 */
router.get('/:id', (req, res) => {
  /**
   * Get Tv Serie
   */
  TvSeries
    .find({ _id: req.params.id })
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .populate('tvSerieComments episodes seasons')
    .lean()
    .then((ts) => {
      if (ts) {
        res.json(ts);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});
module.exports = router;
