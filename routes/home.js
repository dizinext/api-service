/**
 * Get dependencies
 */
const express = require('express'),
      router = express.Router();

/**
 * / - GET
 */
router.get('/', (req, res) => {
  res.json({
    "apiStatus": "ok"
  });
});

module.exports = router;
