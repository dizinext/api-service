/**
 * Get dependencies
 */
const express = require('express'),
      bcrypt = require('bcryptjs'),
      User = require('../models/User'),
      Utils = require('../lib/utils'),
      router = express.Router();

/**
 * /users?page=[Page Number - Starts from 0] - GET
 *
 * Note: This function will return 20 items
 */
router.get('/', (req, res) => {

  /**
   * Find Users
   */
  User
    .find({}, '-password -__v')
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .populate('posts')
    .lean()
    .then((users) => res.json(users))
    .catch((err) => {
      console.log(err);
      res.json({ "error": true });
    });
});

/**
 * /users/new - POST
 * Datas: name, username, password
 *
 * Notes:
 * 	- Password will be hashed with bcrypt
 */
router.post('/new', (req, res) => {

  /**
   * Save User
   */
  // Hashing Password
  var hash = bcrypt.hashSync(req.body.password, 8),
      // Creating User
      newUser = new User({
        name: req.body.name,
        username: req.body.username,
        premiumUser: req.body.premiumUser,
        password: hash
      });

  newUser
    .save()
    .then(users => res.json({ "success": true, "user": newUser }))
    .catch((err) => {
      console.log(err);
      res.json({ "success": false, error: err })
    });
});

/**
 * /users/{username} - GET
 * Datas: username
 */
router.get('/:username', (req, res) => {
  /**
   * Get User
   */
  User
    .findOne({username: req.params.username}, '-password -__v')
    .populate('posts')
    .lean()
    .then((users) => {
      if (users) {
        res.json(users);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});

/**
 * /users/edit/{username} - GET
 * Sendable Datas: username, name, password, editor
 */
router.post('/edit/:username', (req, res) => {
  /**
   * Get User
   */
  User
    .findOne({username: req.params.username})
    .then((user) => {

      // If we have changes for user in reques
      if (!Utils.isEmptyObject(req.body)) {

        // User Found
        if (user) {

          // Check user if edited profile self profile
          if (req.params.username === req.decoded.username) {

            for (var value in req.body) {

              // Looking for changes
              if (req.body[value] !== user[value]) {
                user[value] = req.body[value];
              }
            }

            user
              .save()
              .then(newUser => {
                res.json(newUser);
              })
              .catch(() => res.json({ "error": true, "message": "Error occured while saving" }));

          } else {
            res.json({ "error": true, "message": "You can't edit this profile." });
          }
        } else {
          res.json({ "error": true, "message": "User not found." });
        }
      } else {
        res.json({ "error": true, "message": "No changes." });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});

/*
 * Debug Functions
 */
if (global.debug) {

  /**
   * /users/pwCheck - POST
   * Datas: username, password
   */
  router.post('/pwCheck', (req, res) => {

    /**
     * Check User Password
     */
    User
      .findOne({username: req.body.username})
      .then((users) => {
        let hash = bcrypt.compareSync(req.body.password, users.password);
        if (hash) {
          res.json(users);
        } else {
          res.json({ "error": true });
        }
      })
      .catch((err) => {
        console.log(err);
        res.json({ "error": true });
      });
  });
}

module.exports = router;
