/**
 * Get dependencies
 */
const express = require('express'),
      WatchLater = require('../models/WatchLater'),
      router = express.Router();

/**
 * /watch-later/new/:id - POST
 * Datas: _episode: episode id, _saver, note
 */
router.post('/new/:id', (req, res) => {

  // Creating Episode
  var newWl = new WatchLater({
    _episode: req.params.id,
    _saver: req.decoded.username,
    note: req.body.note
  });

  newWl
    .save()
    .then(wl => res.json({ "success": true, "Watch Later": wl }) )
    .catch((err) => {
      console.log(err);
      res.json({
        "success": false,
        error: err
      });
    });
});

/**
 * /watch-later/?page=[Page Number - Starts from 0] - GET
 * Datas: id
 */
router.get('/', (req, res) => {

  /**
   * Get Saved Episodes for watching later
   */
  WatchLater
    .find({ _saver: req.decoded.username })
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .populate('episode')
    .lean()
    .then((ts) => {
      if (ts) {
        res.json(ts);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});

module.exports = router;
