/**
 * Get dependencies
 */
const express = require('express'),
      User = require('../models/User'),
      bcrypt = require('bcryptjs'),
      jwt = require('jsonwebtoken'),
      router = express.Router();

/**
 * /authenticate/new - POST
 */
router.post('/new', (req, res) => {

  // Find the user
  User
    .findOne({username: req.body.username})
    .then((user) => {

      // Check user exist
      if (user) {

        // If password match
        if (bcrypt.compareSync(req.body.password, user.password)) {

          // Creating Token with jsonwebtoken
          var token = jwt.sign({username: user.username}, req.app.get('secretKey'), {
            expiresIn: "7 days"
          });

          user.token = token;
          user
            .save()
            .then((user) => {
              if (user) {
                res.json({
                  success: true,
                  message: "Token created!",
                  token: token
                });
              } else {
                res.json({
                  success: false,
                  data: 'Error occurred while saving user token'
                });
              }
            });
        } else {
          res.json({
            error: true,
            message: "Wrong Password"
          });
        }
      } else {
        res.json({
          error: true,
          message: "User not found"
        });
      }
    })
    .catch((err) => {
      res.json({ error: true })
    });
});

/**
 * /authenticate/check - POST
 */
router.post('/check', (req, res) => {

  // Find the user
  User
    .findOne({username: req.body.username})
    .then((user) => {

      // Check user exist
      if (user) {

        // If password match
        if (bcrypt.compareSync(req.body.password, user.password)) {

          // Compare with token in db.
          // Checking for token requester is same user
          if (req.body.token === user.token) {

            // verifies secret and checks exp
            jwt.verify(req.body.token, req.app.get('secretKey'), (err, decoded) => {
              if (err) {
                res.json({
                  success: false,
                  message: "Failed to authenticate token."
                });
              } else {
                res.json({
                  success: true,
                  message: "Correct Token!"
                });
              }
            });
          } else {
            res.json({
              error: true,
              message: "Failed to authenticate with user."
            });
          }
        } else {
          res.json({
            error: true,
            message: "Wrong Password"
          });
        }
      } else {
        res.json({
          error: true,
          message: "User not found"
        });
      }
    })
    .catch((err) => {
      res.json({ error: true })
    });
});

module.exports = router;
