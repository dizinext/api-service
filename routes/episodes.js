/**
 * Get dependencies
 */
const express = require('express'),
      Episodes = require('../models/Episode'),
      router = express.Router();

/**
 * /episode/new/:id - POST
 * Datas: _ts_id, _creator, name, videoUrls, subtitleUrls
 */
router.post('/new/:id', (req, res) => {

  // Creating Episode
  var newEpisode = new Episodes({
    _ts_id: req.params.id,
    _season_id: req.body.seasonId,
    name: req.body.name,
    videoUrls: req.body.videoUrls,
    subtitleUrls: req.body.subtitleUrls
  });

  newEpisode
    .save()
    .then(ts => res.json({ "success": true, "episode": ts }) )
    .catch((err) => {
      console.log(err);
      res.json({
        "success": false,
        error: err
      });
    });
});

/**
 * /episode/{id}?page=[Page Number - Starts from 0] - GET
 * Datas: id
 */
router.get('/:id', (req, res) => {
  /**
   * Get Episode
   */
  Episodes
    .find({ _ts_id: req.params.id })
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .populate('episodeComments')
    .lean()
    .then((ts) => {
      if (ts) {
        res.json(ts);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});

/**
 * /episode/detail/{id}?page=[Page Number - Starts from 0] - GET
 * Datas: id
 */
router.get('/detail/:id', (req, res) => {
  /**
   * Get Episode
   */
  Episodes
    .findOne({ _id: req.params.id })
    .populate('episodeComments')
    .lean()
    .then((ts) => {
      if (ts) {
        res.json(ts);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});
module.exports = router;
