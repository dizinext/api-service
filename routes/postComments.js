/**
 * Get dependencies
 */
const express = require('express'),
      PostComment = require('../models/PostComment'),
      router = express.Router();

//
// TODO: Like Comment
//

/**
 * /post-comments/{id}?page=[Page Number - Starts from 0] - GET
 * Datas: id
 */
router.get('/:id', (req, res) => {
  /**
   * Get User
   */
  PostComment
    .find({ _post_id: req.params.id }, "-__v")
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .populate('postComments')
    .then((posts) => {
      if (posts) {
        res.json(posts);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});

/**
 * /post-comments/user/{username}?page=[Page Number - Starts from 0] - GET
 * Datas: username
 */
router.get('/user/:username', (req, res) => {
  /**
   * Get User
   */
  PostComment
    .find({ _creator: req.params.username }, "-__v")
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .populate('postComments')
    .then((posts) => {
      if (posts) {
        res.json(posts);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});

/**
 * /post-comments/new/{id} - POST
 * Datas: postId, senderUsername, text
 */
router.post('/new/:id', (req, res) => {

  // Creating Post Comment
  var newPostComment = new PostComment({
    _post_id: req.params.id,
    _creator: req.body.senderUsername,
    text: req.body.text
  });

  newPostComment
    .save()
    .then(posts => res.json({ "success": true, "post": newPostComment }) )
    .catch((err) => {
      console.log(err);
      res.json({
        "success": false,
        error: err
      });
    });
});

module.exports = router;
