/**
 * Get dependencies
 */
const express = require('express'),
      TvSerieComment = require('../models/TvSerieComment'),
      router = express.Router();

//
// TODO: Like Comment
//

/**
 * /tv-series-comments/{id}?page=[Page Number - Starts from 0] - GET
 * Datas: id
 */
router.get('/:id', (req, res) => {
  /**
   * Get User
   */
  TvSerieComment
    .find({ _ts_id: req.params.id }, "-__v")
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .populate('_ts_id')
    .lean()
    .then((posts) => {
      if (posts) {
        res.json(posts);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});

/**
 * /tv-series-comments/new/{id} - POST
 * Datas: postId, senderUsername, text
 */
router.post('/new/:id', (req, res) => {

  // Creating Post Comment
  var NewTvSerieComment = new TvSerieComment({
    _ts_id: req.params.id,
    _creator: req.body.senderUsername,
    text: req.body.text
  });

  NewTvSerieComment
    .save()
    .then(posts => res.json({ "success": true, "post": NewTvSerieComment }) )
    .catch((err) => {
      console.log(err);
      res.json({
        "success": false,
        error: err
      });
    });
});

module.exports = router;
