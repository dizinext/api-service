/**
 * Get dependencies
 */
const express = require('express'),
      EpisodeComment = require('../models/EpisodeComment'),
      router = express.Router();

//
// TODO: Like Comment
//

/**
 * /episode-comments/{id}?page=[Page Number - Starts from 0] - GET
 * Datas: id
 */
router.get('/:id', (req, res) => {
  /**
   * Get User
   */
  EpisodeComment
    .find({ _ts_id: req.params.id }, "-__v")
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .lean()
    .then((comments) => {
      if (comments) {
        res.json(comments);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});

/**
 * /episode-comments/new/{id} - POST
 * Datas: postId, senderUsername, text
 */
router.post('/new/:id', (req, res) => {

  // Creating Post Comment
  var NewEpisodeComment = new EpisodeComment({
    _ep_id: req.params.id,
    _creator: req.body.senderUsername,
    text: req.body.text
  });

  NewEpisodeComment
    .save()
    .then(posts => res.json({ "success": true, "post": NewEpisodeComment }) )
    .catch((err) => {
      console.log(err);
      res.json({
        "success": false,
        error: err
      });
    });
});

module.exports = router;
