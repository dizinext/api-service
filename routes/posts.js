/**
 * Get dependencies
 */
const express = require('express'),
      Post = require('../models/Post'),
      router = express.Router();

/**
 * /posts?page=[Page Number - Starts from 0] - GET
 *
 * Note: This function will return 20 items
 */
router.get('/', (req, res) => {

  /**
   * Find Posts
   */
  Post
    .find({}, "-__v")
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .lean()
    .then((posts) => res.json(posts))
    .catch((err) => {
      console.log(err);
      res.json({ "error": true });
    });
});

/**
 * /posts/new - POST
 * Datas: _creator, text
 */
router.post('/new', (req, res) => {

  // Creating Post
  var newPost = new Post({
    _creator: req.body.username,
    text: req.body.text
  });

  newPost
    .save()
    .then(posts => res.json({ "success": true, "post": newPost }) )
    .catch((err) => {
      console.log(err);
      res.json({
        "success": false,
        error: err
      });
    });
});

/**
 * /posts/{username}?page=[Page Number - Starts from 0] - GET
 * Datas: username
 */
router.get('/:username', (req, res) => {
  /**
   * Get User
   */
  Post
    .find({ _creator: req.params.username }, "-__v")
    .skip(Number(req.query.page) * 20)
    .limit(20)
    .lean()
    .then((posts) => {
      if (posts) {
        res.json(posts);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});

/**
 * /posts/id/{id} - GET
 * Datas: id
 *
 * Note: This function will return 20 items
 */
router.get('/id/:id', (req, res) => {
  /**
   * Get User
   */
  Post
    .find({ _id: req.params.id }, "-__v")
    .then((posts) => {
      if (posts) {
        res.json(posts);
      } else {
        res.json({ "error": true });
      }
    })
    .catch((err) => {
      log(err);
      res.json({ "error": true });
    });
});

module.exports = router;
